# transmission-docker

A light-weight docker image for running transmission daemon.

## What is transmission?

Transmission is a cross-platform BitTorrent client that is open source, easy to use and feature-rich. [More info](https://transmissionbt.com/about/)

## Why run transmission as a docker container?

Transmission can be installed on local machines as it is cross-platform and has both terminal and GUI options. However, using transmission in a docker container helps us create our own BitTorrent server where we can access transmission server from any connected device using either the WebUI (using http://<server address>:<port>) or we can use one of the frontend remote control clients (list: [Transmission Remote Controls](https://transmissionbt.com/resources/))

Hence, it becomes easier to manage the downloaded content on your server. 

## How to access the transmission-docker image?

There are 2 ways: 
- build the image yourself.
- download the image using the local repo registry (`docker pull registry.gitlab.com/roh93/transmission-docker:latest`)

_Note:_ The file [settings.json](settings.json) contains the options that are passed onto transmission-daemon. Feel free to edit it if needed before building the image. Details of settings parameters can be found in [transmission wiki](https://github.com/transmission/transmission/wiki/Editing-Configuration-Files)

The image available in the registry is built using the default values available in the settings file. 

## How to run transmission-docker container?

It is recommended to use [docker-compose](https://docs.docker.com/compose/) to run the container. 

### Docker-compose

Make sure that you specify the specify the volume directories under volumes section. You can optionally specify the username and password for the transmission daemon using environment values. If nothing is specified, the default credentials are used. 

```
    environment:
      - USER=admin #optional
      - PASS=admin #optional
    volumes:
      - <path to work dir>:/transmission/work
      - <path to data dir>:/data
      - <path to watchdir dir>:/watchdir
```
Details of different directories are given below:
- work-dir = This is the work directory of transmission-daemon which is used for dumping the logs and settings. It is useful for troubleshooting. 
- data-dir = This is the main directory where the torrents are downloaded to. Transmission creates 2 sub-directories under this named as `Downloads`(where completed downloads are saved) and `Incomplete` (where incomplete downloads are stored)
- watch-dir = This directory is monitored by transmission for any torrent files. If the torrent file is found, the torrent is added by transmission and it starts downloading. 

By default, we expose ports 9091 for transmission-daemon and 51413 for peer sockets. This can be edited in docker-compose: 
```
    ports:
      - 9091:9091
      - 51413:51413
      - 51413:51413/udp
```

Finally, you can start the docker container using command:

```console
$ docker-compose up --detach
```

In case you require to build/re-build the image before starting the container, you can use the flag `--build` to build the image locally before running the container.

### Docker run

In case you don't want to use docker-compose, you can directly run the docker container using the `docker run` command:
```console
$ docker run --name=transmission -d \
            -v /path/to/workdir:/transmission/work \
            -v /path/to/datadir:/data \
            -v /path/to/watchdir:/workdir \
            -e USER=your-user \
            -e PASS=your-pass \
            -p 9091:9091 \
            -p 51413:51413 \
            -p 51413:51413/udp \
            --restart=unless-stopped \
            registry.gitlab.com/roh93/transmission-docker
```

Make sure to specify the required directories and specify USER and PASS values only if you intend to set your own user/pass for accessing transmission (Highly recommended!)
