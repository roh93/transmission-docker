FROM alpine:latest
LABEL maintainer Rohith N <rohithn993@gmail.com>

# install transmission
RUN apk --no-cache --no-progress update && \
    apk --no-cache --no-progress add transmission-daemon

ENV TRANSMISSION_HOME /transmission

RUN mkdir -p "$TRANSMISSION_HOME"/work && \
    mkdir -p /data && \
    mkdir -p /watchdir

COPY settings.json /

COPY transmission-start.sh /

EXPOSE 9091 51413/tcp 51413/udp

ENV USER transmission
ENV PASS transmission

WORKDIR /

CMD [ "sh", "./transmission-start.sh"]