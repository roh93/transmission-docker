#!/bin/sh

# copy the settings file from the image to work dir
if [ ! -f "$TRANSMISSION_HOME/work/settings.json" ]; then
    cp /settings.json "$TRANSMISSION_HOME"/work
fi

transmission-daemon -f --username="$USER" --password="$PASS" --config-dir="$TRANSMISSION_HOME"/work --logfile="$TRANSMISSION_HOME"/work/transmission.log